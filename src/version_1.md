# Version 1

## Headers

### **icy-pub** _[Number]_

This tag is used to specify if the stream should be indexed in shoutcasts own index.

Possible values are 0 and 1. 0 means NOT public. 1 means public.

### **icy-audio-info** _[String]_

This is a multivalue field which can have one or multiples of the following items:

* sample rate
* quality
* channels
* bitrate

Examples:

* icy-audio-info: ice-samplerate=44100;ice-bitrate=128;ice-channels=2
* icy-audio-info: ice-samplerate=22050;ice-bitrate=64;ice-channels=1

### **content-type** _[String]_

Stream encoding type. More details at [mozilla](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type)

Examples:

* content-type: audio/flac
* content-type: audio/mpeg
* content-type: audio/ogg
* content-type: video/ogg

More examples at [IANA](https://www.iana.org/assignments/media-types/media-types.xhtml)

### **icy-name** _[String]_

Name of the stream or the station. It should be short to the point because it will most likely be displayed in lists with many streams.

Examples:

* icy-name: Smurf City 88.5
* icy-name: Good vibrations radio

### **icy-description** _[String]_

A longer description of a station.

Examples:

* icy-description: The number 1 stream of smurf city!
* icy-description: We are all made of vibrations.

### **icy-url** _[String]_

Homepage of the stream. This is not the stream url, but some kind of stream homepage.

Examples:

* icy-url: http://www.smurf-town.com
* icy-url: https://www.good-vibrations-radio.com
* icy-url: https://www.channel1radio.com/jazz/

### **icy-br** _[Number]_

Bitrate as a number in kilobits per second.

Examples:

* icy-br: 128
* icy-br: 256

### **icy-genre** _[String]_

Multiple tags split up by comma that describe the station.

Examples:

* icy-genre: jazz,classical
* icy-genre: pop

### **icy-sr** _[Number]_

Sampling rate of the stream in Hz.

Examples:

* icy-sr: 44100
* icy-sr: 22050
