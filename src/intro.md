# Intro

Identification of the content of internet streams was not well cared for in the past. Also there is no complete specification for the current way of adding information to the stream. This website tries to solve this and it is planned as a primary source for internet stream metainformation.

If you want to contribute to this document, please make a pull request to the [gitlab](https://gitlab.com/radiobrowser/stream-meta-info) project.

## External links

* [Icecast server](http://icecast.org/)
* [Shoutcast server](https://www.shoutcast.com/)
* [Steamcast server](http://www.steamcast.com/)
* [Cast server](https://github.com/Innovate-Technologies/Cast)
