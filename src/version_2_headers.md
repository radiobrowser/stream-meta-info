## Headers

### **icy-logo** _[String]_

Url of a logo for this stream, should be in JPG or PNG format. (e.g.: http://example.com/logo.png)

### **icy-main-stream-url** _[String]_

Link to the main url of this stream. This may be used by stream providers to direct indexers to the main publicly exposed url. Indexers should update their database accordingly. May also be used to redirect the users to a load balanced version of the stream.

### **icy-version** _[Number]_

The version of this header. 1 is the default. 2 is an extension to the default which is compatible to 1 but adds more headers.

### **icy-index-metadata** _[Number]_

Use all of header metadata. This is mainly used to force indexers to update their information. 0 means NO. 1 means YES.
The problem this header tries to solve is the following:

* A lot of streams do have wrong metadata set. This is because a lot of operators just keep the default values of the server software.
* Automatic indexers that use this information to override their database, should know that they actually can trust the information.
* For that this header should always be 0 by default. It has to be a conscious decision to set it to 1.

### **icy-country-code** _[String]_

The 2 letter code of the country the stream is located in. Legal values are specified in ISO 3166-1.

Examples:

* icy-country-code: AT
* icy-country-code: DE
* icy-country-code: UK

More examples on [wikipedia](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)

### **icy-country-subdivison-code** _[String]_

Code that specifies the subdivision of the country this stream is located in. Legal values are specified in ISO 3166-2.

Examples:

* icy-country-subdivison-code: AT-1
* icy-country-subdivison-code: AM-ER

More examples on [wikipedia](https://en.wikipedia.org/wiki/ISO_3166-2)

### **icy-language-codes** _[String]_

This is a multivalue field because streams can use multiple languages. It should be sorted starting from the most used to the least used.
It does accept 2 letter codes (ISO-639-1) and 3 letter codes (ISO-639-3)

Examples:

* icy-language-codes: en
* icy-language-codes: de
* icy-language-codes: eng,deu
* icy-language-codes: zh,cmn

More examples on wikipedia [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) [ISO 639-3](https://en.wikipedia.org/wiki/ISO_639-3).

### **icy-geo-lat-long** _[Number]_

Geo information, latitude and longitude of the streaming station. Can be used to map the stream to a point on the globe.

Examples:

* icy-geo-lat-long: 40.684144,-20.37592
* icy-geo-lat-long: 10.2349872, 33.21974

### **icy-do-not-index** _[Number]_

If a stream operator wants this stream to be absolutely private, this option can be set to 1.
The default value is 0.

Examples:

* icy-do-not-index: 1
* icy-do-not-index: 0
