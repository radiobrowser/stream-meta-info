## Meta-data files

A lot of times the operator of the stream does not have the means of adding new headers to the stream, because they use some kind of distribution provider. This way they don't have to care so much about getting the stream to the enduser but the configuration systems of those providers are often quite limited.

To fix this problem we assume the following:

* It is possible for the stream owner to change at least the homepage information of version 1 headers. (The information in the header field "icy-url")
* They are able to put an additonal file on this website. 

### Stream meta-data file

The information in this file is basically the same as in the headers. But the information in this file overrides the headers and extends it, where the headers do not support the highest version.
The filename is always built using the "icy-url" and the filename "streaminfo.json".

Examples:
* an icy-url of "http://my.superradio.com" will produce:

  http://my.superradio.com/streaminfo.json
* an icy-url of "https://goodstreaming.com/channel1" will produce:

  https://goodstreaming.com/channel1/streaminfo.json

The field names are exactly the same as the header names.

Example content of the file streaminfo.json:

```json
{
    "icy-index-metadata": 1,
    "icy-version": 2,
    "icy-name": "Super radio",
    "icy-description": "This is the super radio of the world.",
    "icy-genre": "jazz,classical",
    "icy-language-codes": "eng,deu",
    "icy-country-code": "AT",
    "icy-country-subdivison-code": "AT-1",
    "icy-logo": "https://super.radio.com/favicon.png"
}
```

If you want your stream to to be removed from any index you have to use the following file:

```json
{
    "icy-index-metadata": 1,
    "icy-version": 2,
    "icy-do-not-index": 1
}
```
