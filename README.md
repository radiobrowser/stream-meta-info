# stream-meta-info
Internet stream metainformation documentation. This project is the source for https://www.stream-meta.info

## Build
This webpage uses mdbook static page generator.

```
cargo install mdbook
mdbook build
```